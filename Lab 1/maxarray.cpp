/*-------------------------------------------------------------------------
   Program to illustrate the use of a function template to display
   an array with elements of any type for which << is defined. 

   Output:  An array of ints and an array of doubles using maxArray()
 -------------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <cmath>

template <class ElementType>
ElementType maxArray(ElementType array[], int length, int n);

int main()
{

}

template <class ElementType>
ElementType maxArray(ElementType array[], int first, int last)
{
    int midIndex = first + (last - first) / 2;
    ElementType result;

    if (first == last)
        result = array[first];
    else {
        result = std::max(maxArray(array, first, midIndex), maxArray(array, midIndex + 1, last));
    }

    return result;
}
