#include "gtest/gtest.h"
#include "../maxarray.cpp"

TEST(basicChecks, testMaxArray)
{
    int x[] = {1333, 245, 97, 20, 67};
    EXPECT_EQ(1333, maxArray(x, 1, 5));
}