/*
 * Ian Harmon
 *
 * Lab 1
 *
 * COSC 2436
 */

#include <iostream>
#include <string>

template <class ElementType>
void maxArray(ElementType array[], int numElements);

int main()
{
    double x[] = {1.1, 2.2, 3.3, 4.4, 5.5};
    maxArray(x, 5);
    int num[] = {1, 2, 3, 4};
    maxArray(num, 4);
    std::string s[] = {"aa", "bb", "cc"};
    maxArray(s, 3);

}

/*-------------------------------------------------------------------------
  Display elements of any type (for which the output operator is defined)
  stored in an array.

  Precondition:  ElementType is a type parameter.
  Postcondition: First numElements of array have been output to cout.
 ------------------------------------------------------------------------*/
template <class ElementType>
void maxArray(ElementType *array, int numElements)
{
    for (int i = 0; i < numElements; i++)
        std::cout << array[i] << "  ";
    std::cout << std::endl;
}