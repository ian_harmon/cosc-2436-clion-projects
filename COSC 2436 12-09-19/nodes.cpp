#include <iostream>

class Node {
private:
    int item;
    Node *next;

public:
    Node(int item) : item(item), next(nullptr) {}

    int getItem() const {
        return item;
    }

    void setItem(int item) {
        Node::item = item;
    }

    Node *getNext() const {
        return next;
    }

    void setNext(Node *next) {
        Node::next = next;
    }
};

// Bag has 1 or more nodes, therefore relationship between them is considered composition

class Bag {
private:
    Node *head;

public:
    Bag(): head(nullptr) {}

    ~Bag() {
        std::cout << "I am a bag, and I have died. :(" << std::endl;

        while(head != nullptr) {
            Node *delNode = head;
            head = head->getNext();
            delete delNode;
        }
    }

    void add(int number) {
        Node *newNode = new Node(number);
        newNode->setNext(head);             // -> is the dot operator for a pointer; therefore set the "next" attribute of newNode to "head"
        head = newNode;
    }

    /*
     * 1) make current (the found value) the head value
     * 2) point head to the next node
     * 3) delete the original head value and pointer
     */

    void remove(int number) {
        Node *current = head;
        while(current != nullptr && current->getItem() != number) {
            current = current->getNext();
        }

        if(current != nullptr) {
            current->setItem(head->getItem());
            Node *delNode = head;
            head = head->getNext();
            delete delNode;
            delNode = nullptr;
        }
    }

    void display() {
        Node *current = head;
        std::cout << "Items in bag: ";
        while(current != nullptr) {
            std::cout << current->getItem() << " ";
            current = current->getNext();
        }
        std::cout << std::endl;
    }
};

int main() {

    Bag b;

    b.display();

    b.add(5);
    b.add(7);
    b.add(9);

    b.display();

    b.remove(7);

    b.display();

    return 0;
}