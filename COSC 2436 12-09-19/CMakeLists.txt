cmake_minimum_required(VERSION 3.14)
project(COSC_2436_12_09_19)

set(CMAKE_CXX_STANDARD 14)

add_executable(COSC_2436_12_09_19 nodes.cpp)