// Ian Harmon

#include <cctype>
#include <iostream>
#include <stdexcept>
#include <string>

#define DEBUG_BUILD

const int MAX_STACK = 5;

class PreconditionViolatedException : public std::logic_error
{
public:
    PreconditionViolatedException(const std::string& message) : std::logic_error("Precondition Violated Exception: " + message)
    {
    }
};

class Node {
private:
    int item;
    Node *next;

public:
    Node(const int item) : item(item), next(nullptr) {}

    Node() : item(item), next(nullptr) {}

    Node(const int &anItem, Node* nextNodePtr) : item(anItem), next(nextNodePtr) {}

    int getItem() const {
        return item;
    }

    void setItem(int item) {
        Node::item = item;
    }

    Node *getNext() const {
        return next;
    }

    void setNext(Node *next) {
        Node::next = next;
    }
};

class PostFixCalculator {
private:
    Node* head;

    bool isEmpty() const {
        return head == nullptr;
    }

    bool push(const int &newEntry) {
        Node* newNodePtr = new Node(newEntry, head);
        head = newNodePtr;
        newNodePtr = nullptr;

        return true;
    }

    bool pop() {
        bool result = false;
        if (!isEmpty())
        {
            Node* nodeToDeletePtr = head;
            head = head->getNext();

            // Return deleted node to system
            nodeToDeletePtr->setNext(nullptr);
            delete nodeToDeletePtr;
            nodeToDeletePtr = nullptr;

            result = true;
        }

        return result;
    }

    int peek() const {
        if (isEmpty())
            throw(PreconditionViolatedException("peek() called with empty stack."));

        // Stack is not empty; return top
        return head->getItem();
    }

public:
    PostFixCalculator() : head(nullptr) {}

    PostFixCalculator(const PostFixCalculator& aStack)
    {
        // Point to nodes in original chain
        Node* origChainPtr = aStack.head;

        if (origChainPtr == nullptr)
            head = nullptr;  // Original stack is empty
        else
        {
            // Copy first node
            head = new Node();
            head->setItem(origChainPtr->getItem());

            // Point to last node in new chain
            Node* newChainPtr = head;

            // Advance original-chain pointer
            origChainPtr = origChainPtr->getNext();

            // Copy remaining nodes
            while (origChainPtr != nullptr)
            {
                // Get next item from original chain
                char nextItem = origChainPtr->getItem();

                // Create a new node containing the next item
                Node* newNodePtr = new Node(nextItem);

                // Link new node to end of new chain
                newChainPtr->setNext(newNodePtr);

                // Advance pointer to new last node
                newChainPtr = newChainPtr->getNext();

                // Advance original-chain pointer
                origChainPtr = origChainPtr->getNext();
            }

            newChainPtr->setNext(nullptr);
        }
    }

    ~PostFixCalculator()
    {
        clear();
    }

    void evaluateExpression(std::string s) {
        int result;
        for (int i = 0; i < s.length(); i++) {
            if (isdigit(s[i])) {
                push(s[i] - '0');

                #ifdef DEBUG_BUILD
                std::cout << "Pushed: " << s[i] << std::endl;
                #endif

            } else {
                int operand2 = peek();
                pop();
                int operand1 = peek();
                pop();

                #ifdef DEBUG_BUILD
                std::cout << "Performing " << operand1;
                #endif

                switch (s[i]) {
                    case '+':

                        #ifdef DEBUG_BUILD
                        std::cout << " + ";
                        #endif

                        result = operand1 + operand2;
                        break;
                    case '-':

                        #ifdef DEBUG_BUILD
                        std::cout << " - ";
                        #endif

                        result = operand1 - operand2;
                        break;
                    case '/':

                        #ifdef DEBUG_BUILD
                        std::cout << " / ";
                        #endif

                        result = operand1 / operand2;
                        break;
                    case '*':

                        #ifdef DEBUG_BUILD
                        std::cout << " * ";
                        #endif

                        result = operand1 * operand2;
                        break;
                }

                #ifdef DEBUG_BUILD
                std::cout << operand2 << std::endl;
                #endif

                push(result);
            }
            #ifdef DEBUG_BUILD
            display();
            std::cout << std::endl;
            #endif
        }

        #ifdef DEBUG_BUILD
        std::cout << "Result of evalutation: " << peek() << std::endl;
        #endif
    }

    void clear() {
        while (!isEmpty())
            pop();
    }

    void display() {
        Node *current = head;
        while(current != nullptr) {
            std::cout << current->getItem();
            current = current->getNext();
        }
    }
};

int main() {
    PostFixCalculator calc;

    std::string input;

    std::cout << "Please type a valid postfix expression." << std::endl
              << "\"Stack\" is the result of the previous expression." << std::endl
              << "Type \"clear\" to delete the current stack." << std::endl
              << "Type \"quit\" to close the application." << std::endl;

    do {
        std::cout << "INPUT: ";
        getline(std::cin, input);

        if (input == "clear") {
            calc.clear();
        } else {
            calc.evaluateExpression(input);
        }

        std::cout << "Current stack: "; calc.display(); std::cout << std::endl;

    } while (input != "quit");

    return 0;
}