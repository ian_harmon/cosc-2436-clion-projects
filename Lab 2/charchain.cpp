#include <iostream>

class Node {
private:
    char item;
    Node *next;

public:
    Node(const char item) : item(item), next(nullptr) {}

    Node() : item(item), next(nullptr) {}

    char getItem() const {
        return item;
    }

    void setItem(char item) {
        Node::item = item;
    }

    Node *getNext() const {
        return next;
    }

    void setNext(Node *next) {
        Node::next = next;
    }
};

class LinkedChar {
private:
    Node *head;

    Node *getTail() {

        Node *current = head;

        while (current->getNext() != nullptr) {
            current = current->getNext();
        }

        return current;
    }

    void appendChar(char newChar) {
        Node *newNode = new Node(newChar);

        if(head == nullptr)
            head = newNode;

        getTail()->setNext(newNode);

        newNode->setNext(nullptr);
    }

    void pushChar(char newChar) {
        Node *newNode = new Node(newChar);
        newNode->setNext(head);
        head = newNode;
    }

    /*
     *    removeChar()
     * 1) make current (the found value) the head value
     * 2) point head to the next node
     * 3) delete the original head value and pointer
     */

    void removeChar(char delChar) {
        Node *current = head;
        while(current != nullptr && current->getItem() != delChar) {
            current = current->getNext();
        }

        if(current != nullptr) {
            current->setItem(head->getItem());
            Node *delNode = head;
            head = head->getNext();
            delete delNode;
            delNode = nullptr;
        }
    }

public:
    LinkedChar(): head(nullptr) {}

    LinkedChar(const char c): head(nullptr) {
        appendChar(c);
    }

    LinkedChar(const std::string s): head(nullptr) {
        appendString(s);
    }

    LinkedChar(const LinkedChar& aLinkedChar): head(nullptr) {
        appendLinkedChar(aLinkedChar);
    }

    ~LinkedChar() {
        clear();
    }

    void appendString(std::string s) {
        int sCount = 0;
        char currChar = s[sCount];
        while(currChar != '\0') {
            appendChar(currChar);
            ++sCount;
            currChar = s[sCount];
        }
    }

    void appendLinkedChar(const LinkedChar &lc) {
        Node *newNode = lc.head;
        while (newNode != nullptr)
        {
            // Create a new node containing the next item
            Node* newNodePtr = new Node(newNode->getItem());

            // Link new node to end of current LinkedChar
            getTail()->setNext(newNodePtr);

            // Advance new node
            newNode = newNode->getNext();
        }
        getTail()->setNext(nullptr);
    }

    int length() const {
        int count = 0;
        Node *current = head;
        while(current != nullptr) {
            current = current->getNext();
            ++count;
        }

        return count;
    }

    int index(char ch) const {
        int count = 0;
        Node *current = head;
        while(current != nullptr && current->getItem() != ch) {
            current = current->getNext();
            ++count;
        }

        if (current == nullptr) {
            return -1;
        }

        return count;
    }

    bool submatch(const LinkedChar &lc) const {
        bool started = false;

        Node *thisNode = head;
        Node *subNode = lc.head;
        while (thisNode != nullptr && subNode != nullptr) {
            if (thisNode->getItem() == subNode->getItem()) {
                subNode = subNode->getNext();
                if (!started)
                    started = true;
            } else
            if (started)
                return false;

            thisNode = thisNode->getNext();
        }

        return true;
    }

    void clear() {
        while(head != nullptr) {
            Node *delNode = head;
            head = head->getNext();
            delete delNode;
        }
    }

    void reverse()
    {
        Node* current = head;
        Node *prev = nullptr, *next = nullptr;

        while (current != nullptr) {
            next = current->getNext();

            current->setNext(prev);

            prev = current;
            current = next;
        }
        head = prev;
    }

    void display() {
        Node *current = head;
        while(current != nullptr) {
            std::cout << current->getItem();
            current = current->getNext();
        }
    }
};

int main() {
    LinkedChar lc1;
    LinkedChar lc2;
    std::string input;

    std::cout << "Commands:" << std::endl
              << "new - enter a new string as a linked list" << std::endl
              << "view - view current linked list string" << std::endl
              << "length - get current length of linked list" << std::endl
              << "find - find the 0 based index of a character in the linked list string" << std::endl
              << "append - append a new linked list string to the linked list string" << std::endl
              << "submatch - check if current linked list string contains a given linked list string" << std::endl
              << "clear - clear the current linked list string" << std::endl
              << "quit - close the application" << std::endl;

    do {
        lc2.clear();

        getline(std::cin, input);

        if (input == "new") {
            lc1.clear();
            std::cout << "please enter a linked list string: ";
            std::string s;
            getline(std::cin, s);
            lc1.appendString(s);
            std::cout << "current linked list: ";
            lc1.display();
            std::cout << std::endl;
        } else if (input == "length") {
            std::cout << "length of \"";
            lc1.display();
            std::cout << "\" is: " << lc1.length() << std::endl;
        } else if (input == "clear") {
            lc1.clear();
            std::cout << "linked list cleared" << std::endl;
        } else if (input == "view") {
            std::cout << "current linked list: ";
            lc1.display();
            std::cout << std::endl;
        } else if (input == "find") {
            std::cout << "enter character you wish to find the index of: ";
            std::string ch;

            getline(std::cin, ch);

            if (lc1.index(ch[0]) == -1) {
                std::cout << "linked list \"";
                lc1.display();
                std::cout << "\" doesnt contain " << ch[0] << std::endl;
            } else {
                std::cout << "index of character \'" << ch << "\' of linked list \"";
                lc1.display();
                std::cout << "\" is: " << lc1.index(ch[0]) << std::endl;
            }
        } else if (input == "append") {
            std::cout << "enter linked list string to append to current string: ";
            std::string s;
            getline(std::cin, s);
            lc2.appendString(s);
            lc1.appendLinkedChar(lc2);

            std::cout << "new linked list after append operation:  ";
            lc1.display();
            std::cout << std::endl;
        } else if (input == "submatch") {
            std::cout << "enter linked list string to submatch: ";
            std::string s;
            getline(std::cin, s);
            lc2.appendString(s);

            std::cout << "current linked list ";
            lc1.display();
            if (lc1.submatch(lc2))
                std::cout << " contains linked list ";
            else
                std::cout << " doesn't contain linked list ";
            lc2.display();
            std::cout << std::endl;
        } else {
            std::cout << "command not recognized, try one of the commands listed above" << std::endl;
        }
    } while (input != "quit");

    return 0;
}